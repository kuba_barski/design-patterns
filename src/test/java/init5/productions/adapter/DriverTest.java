package init5.productions.adapter;

import init5.productions.adapter.car.JeepCar;
import init5.productions.adapter.motorbike.SuzukiMotorbike;
import org.junit.jupiter.api.Test;

/**
 * @author Jakub Barski
 */
class DriverTest {

    private static final String LOAD = "Parcel";

    @Test
    void transportLoadWithCar() {
        Driver driver = new Driver(new JeepCar());
        driver.transportLoad(LOAD);
    }

    @Test
    void transportLoadWithMotorbike() {
        Driver driver = new Driver(new MotorbikeAdapter(new SuzukiMotorbike()));
        driver.transportLoad(LOAD);
    }
}