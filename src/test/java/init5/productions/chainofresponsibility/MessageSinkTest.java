package init5.productions.chainofresponsibility;

import init5.productions.chainofresponsibility.message.Message;
import init5.productions.chainofresponsibility.message.SimpleMessage;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Jakub Barski
 */
class MessageSinkTest {
    private MessageSink sut = new MessageSink();

    @ParameterizedTest
    @MethodSource("provider")
    void process(Message message, int handleCount) {
        assertEquals(handleCount, sut.process(message));
    }

    static Stream<Arguments> provider() {
        return Stream.of(
                Arguments.of(SimpleMessage.info("Informational message"), 2),
                Arguments.of(SimpleMessage.warning("Warning message"), 2),
                Arguments.of(SimpleMessage.error("Error message"), 3),
                Arguments.of(SimpleMessage.critical("Critical message"), 4)
        );
    }
}