package init5.productions.bridge.vehicle;

import init5.productions.bridge.engine.DieselEngine;
import init5.productions.bridge.engine.ElectricEngine;
import init5.productions.bridge.engine.PetrolEngine;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

/**
 * @author Jakub Barski
 */
class VehicleTest {

    private static final Logger LOGGER = LogManager.getLogger(VehicleTest.class);

    @ParameterizedTest
    @MethodSource("provider")
    void drive(Vehicle vehicle) {
        LOGGER.info("Testing {} {} with {} engine", vehicle.getModel(), vehicle.getType(), vehicle.getEngineType());

        vehicle.startEngine();
        vehicle.drive();
        vehicle.stopEngine();
    }

    static Stream<Arguments> provider() {
        return Stream.of(
                Arguments.of(new Car("Honda", new ElectricEngine())),
                Arguments.of(new Car("Audi", new DieselEngine())),
                Arguments.of(new Car("Subaru", new PetrolEngine())),
                Arguments.of(new Motorbike("Ducati", new PetrolEngine())),
                Arguments.of(new Motorbike("Zero", new ElectricEngine()))
        );
    }
}