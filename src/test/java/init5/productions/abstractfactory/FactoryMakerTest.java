package init5.productions.abstractfactory;

import init5.productions.abstractfactory.factory.VehicleFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import static init5.productions.abstractfactory.FactoryMaker.VehicleType.*;

/**
 * @author Jakub Barski
 */
class FactoryMakerTest {

    private static final Logger LOGGER = LogManager.getLogger(FactoryMakerTest.class);

    @Test
    void test() {
        logFactoryProducts(FactoryMaker.makeFactory(RACE));
        logFactoryProducts(FactoryMaker.makeFactory(COMMON));
        logFactoryProducts(FactoryMaker.makeFactory(OFFROAD));
    }

    private void logFactoryProducts(VehicleFactory factory) {
        LOGGER.info("{} is driven by {}", factory.produceCar().getModel(), factory.produceDriver().getName());
    }
}