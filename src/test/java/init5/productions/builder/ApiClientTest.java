package init5.productions.builder;

import init5.productions.circuitbreaker.fuse.HardFuse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Jakub Barski
 */
class ApiClientTest {

    private static final Logger LOGGER = LogManager.getLogger(ApiClientTest.class);

    private static final String ENDPOINT_URI = "/endpoint";
    private static URL BASE_URL;

    @BeforeAll
    static void init() throws MalformedURLException {
        BASE_URL = new URL("https://example.com");
    }

    @Test
    void unmodifiedBuilder() {
        ApiClient apiClient = ApiClient.Builder.newBuilder(BASE_URL).build();

        assertEquals(
                "ApiClient{httpVersion=V2, connectionTimeout=5, socketTimeout=5, baseUrl=https://example.com, fuse=DefaultFuse}",
                apiClient.toString()
        );

        apiClient.makeRequest(HttpMethod.POST, ENDPOINT_URI);
    }

    @Test
    void modifiedBuilder() {
        ApiClient apiClient = ApiClient.Builder
                .newBuilder(BASE_URL)
                .setConnectionTimeout(10L)
                .setSocketTimeout(7L)
                .setHttpVersion(HttpVersion.V1_1)
                .setFuse(new HardFuse())
                .build();

        assertEquals(
                "ApiClient{httpVersion=V1_1, connectionTimeout=10, socketTimeout=7, baseUrl=https://example.com, fuse=HardFuse}",
                apiClient.toString()
        );

        apiClient.makeRequest(HttpMethod.PATCH, ENDPOINT_URI);
    }

}