package init5.productions.circuitbreaker;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * @author Jakub Barski
 */
@SuppressWarnings("unchecked")
@ExtendWith(MockitoExtension.class)
class ApiClientTest {
    public static final URI URI = java.net.URI.create("http://example.com");
    public static final HttpRequest REQUEST = HttpRequest.newBuilder(URI).build();

    @Mock
    private HttpClient httpClient;

    @InjectMocks
    private ApiClient sut;

    @Test
    void makeRequest() throws IOException, InterruptedException, ClientException {
        when(httpClient.send(any(HttpRequest.class), any(HttpResponse.BodyHandler.class)))
                .thenThrow(new IllegalArgumentException())
                .thenReturn(null);

        ClientException clientException = assertThrows(ClientException.class, () -> sut.makeRequest(REQUEST));
        assertEquals("Request to API failed!", clientException.getMessage());

        for (int i = 0; i < 3; i++) {
            clientException = assertThrows(ClientException.class, () -> sut.makeRequest(REQUEST));
            assertEquals("Could not make request - blocked by fuse!", clientException.getMessage());
        }

        Thread.sleep(10010);

        sut.makeRequest(REQUEST);

        verify(httpClient, times(2)).send(any(HttpRequest.class), any(HttpResponse.BodyHandler.class));
    }
}