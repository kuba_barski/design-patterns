package init5.productions.circuitbreaker;

import init5.productions.circuitbreaker.fuse.DefaultFuse;
import init5.productions.circuitbreaker.fuse.Fuse;

import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

/**
 * @author Jakub Barski
 */
public class ApiClient {

    private Fuse fuse;
    private HttpClient httpClient;

    public ApiClient(HttpClient httpClient) {
        this.fuse = DefaultFuse.instance();
        this.httpClient = httpClient;
    }

    public void makeRequest(HttpRequest request) throws ClientException {
        if (!fuse.canMakeRequest()) {
            throw new ClientException("Could not make request - blocked by fuse!");
        }

        try {
            httpClient.send(request, HttpResponse.BodyHandlers.discarding());
        } catch (Exception e) {
            fuse.openCircuit();
            throw new ClientException("Request to API failed!", e);
        }
    }
}
