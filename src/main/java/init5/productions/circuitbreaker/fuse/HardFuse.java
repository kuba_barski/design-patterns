package init5.productions.circuitbreaker.fuse;

/**
 * @author Jakub Barski
 */
public class HardFuse implements Fuse {

    @Override
    public boolean canMakeRequest() {
        return true;
    }

    @Override
    public void openCircuit() {

    }

    @Override
    public String toString() {
        return "HardFuse";
    }
}
