package init5.productions.circuitbreaker.fuse;

/**
 * @author Jakub Barski
 */
public interface Fuse {

    boolean canMakeRequest();

    void openCircuit();
}
