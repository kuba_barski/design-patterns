package init5.productions.circuitbreaker.fuse;

import java.util.Objects;

/**
 * @author Jakub Barski
 */
public class DefaultFuse implements Fuse {

    private static final long TEN_SECONDS_IN_NANO = 10_000_000_000L;

    private static volatile DefaultFuse instance;

    private Long openedUntil;

    private DefaultFuse() {
    }

    public static DefaultFuse instance() {
        if (Objects.isNull(instance)) {
            instance = new DefaultFuse();
        }

        return instance;
    }

    @Override
    public boolean canMakeRequest() {
        if (Objects.isNull(openedUntil)) {
            return true;
        }

        if (openedUntil < System.nanoTime()) {
            openedUntil = null;
            return true;
        }

        return false;
    }

    @Override
    public void openCircuit() {
        this.openedUntil = System.nanoTime() + TEN_SECONDS_IN_NANO;
    }

    @Override
    public String toString() {
        return "DefaultFuse";
    }
}
