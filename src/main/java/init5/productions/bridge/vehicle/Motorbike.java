package init5.productions.bridge.vehicle;

import init5.productions.bridge.engine.Engine;
import init5.productions.bridge.engine.EngineType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Jakub Barski
 */
public class Motorbike implements Vehicle {

    private static final Logger LOGGER = LogManager.getLogger(Motorbike.class);

    private String model;
    private Engine engine;

    public Motorbike(String model, Engine engine) {
        this.model = model;
        this.engine = engine;
    }

    @Override
    public void startEngine() {
        LOGGER.info("{} motorbike starting engine", getModel());
        engine.start();
    }

    @Override
    public void stopEngine() {
        LOGGER.info("{} motorbike stopping engine", getModel());
        engine.stop();
    }

    @Override
    public void drive() {
        LOGGER.info("Riding {} motorbike", getModel());
    }

    @Override
    public EngineType getEngineType() {
        return engine.getType();
    }

    @Override
    public String getModel() {
        return model;
    }

    @Override
    public VehicleType getType() {
        return VehicleType.MOTORBIKE;
    }
}
