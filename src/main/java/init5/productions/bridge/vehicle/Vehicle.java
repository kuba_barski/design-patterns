package init5.productions.bridge.vehicle;

import init5.productions.bridge.engine.EngineType;

/**
 * @author Jakub Barski
 */
public interface Vehicle {

    void startEngine();

    void stopEngine();

    void drive();

    EngineType getEngineType();

    String getModel();

    VehicleType getType();
}
