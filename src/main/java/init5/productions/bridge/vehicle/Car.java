package init5.productions.bridge.vehicle;

import init5.productions.bridge.engine.Engine;
import init5.productions.bridge.engine.EngineType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Jakub Barski
 */
public class Car implements Vehicle {

    private static final Logger LOGGER = LogManager.getLogger(Car.class);

    private String model;
    private Engine engine;

    public Car(String model, Engine engine) {
        this.model = model;
        this.engine = engine;
    }

    @Override
    public void startEngine() {
        LOGGER.info("{} car is starting engine", getModel());
        engine.start();
    }

    @Override
    public void stopEngine() {
        LOGGER.info("{} car stopping engine", getModel());
        engine.stop();
    }

    @Override
    public void drive() {
        LOGGER.info("Driving {} car", getModel());
    }

    @Override
    public EngineType getEngineType() {
        return engine.getType();
    }

    @Override
    public String getModel() {
        return model;
    }

    @Override
    public VehicleType getType() {
        return VehicleType.CAR;
    }
}
