package init5.productions.bridge.vehicle;

/**
 * @author Jakub Barski
 */
public enum VehicleType {
    CAR, MOTORBIKE
}
