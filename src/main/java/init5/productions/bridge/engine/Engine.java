package init5.productions.bridge.engine;

/**
 * @author Jakub Barski
 */
public interface Engine {

    void start();

    void stop();

    EngineType getType();
}
