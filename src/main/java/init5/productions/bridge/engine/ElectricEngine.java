package init5.productions.bridge.engine;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Jakub Barski
 */
public class ElectricEngine implements Engine {

    private static final Logger LOGGER = LogManager.getLogger(ElectricEngine.class);

    @Override
    public void start() {
        LOGGER.info("Engine is ready as it is");
    }

    @Override
    public void stop() {
        LOGGER.info("Engine stopped already");
    }

    @Override
    public EngineType getType() {
        return EngineType.ELECTRIC;
    }
}
