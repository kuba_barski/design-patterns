package init5.productions.bridge.engine;

/**
 * @author Jakub Barski
 */
public enum EngineType {
    PETROL, DIESEL, ELECTRIC
}
