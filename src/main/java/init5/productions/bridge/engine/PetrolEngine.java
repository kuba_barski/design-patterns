package init5.productions.bridge.engine;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Jakub Barski
 */
public class PetrolEngine implements Engine {

    private static final Logger LOGGER = LogManager.getLogger(PetrolEngine.class);

    @Override
    public void start() {
        LOGGER.info("Engine starts with roar");
    }

    @Override
    public void stop() {
        LOGGER.info("Engine stops");
    }

    @Override
    public EngineType getType() {
        return EngineType.PETROL;
    }
}
