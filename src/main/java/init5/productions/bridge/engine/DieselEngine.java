package init5.productions.bridge.engine;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Jakub Barski
 */
public class DieselEngine implements Engine {

    private static final Logger LOGGER = LogManager.getLogger(DieselEngine.class);

    @Override
    public void start() {
        LOGGER.info("Engine starts as soon as spark plugs warm up");
    }

    @Override
    public void stop() {
        LOGGER.info("Engine stops as soon as turbo cool down");
    }

    @Override
    public EngineType getType() {
        return EngineType.DIESEL;
    }
}
