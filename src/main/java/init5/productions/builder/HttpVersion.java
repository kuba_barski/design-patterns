package init5.productions.builder;

/**
 * @author Jakub Barski
 */
public enum HttpVersion {
    V1_1, V2
}
