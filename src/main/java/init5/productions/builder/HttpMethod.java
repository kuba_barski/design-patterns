package init5.productions.builder;

/**
 * @author Jakub Barski
 */
public enum HttpMethod {
    POST, GET, PUT, PATCH, DELETE
}
