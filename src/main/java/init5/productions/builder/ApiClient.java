package init5.productions.builder;

import init5.productions.circuitbreaker.fuse.DefaultFuse;
import init5.productions.circuitbreaker.fuse.Fuse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.Optional;

/**
 * @author Jakub Barski
 */
public class ApiClient {

    private static final Logger LOGGER = LogManager.getLogger(ApiClient.class);

    private HttpVersion httpVersion;
    private long connectionTimeout;
    private long socketTimeout;
    private URL baseUrl;
    private Fuse fuse;

    private ApiClient() {
    }

    public void makeRequest(HttpMethod method, String endpointUri) {
        LOGGER.info("Making {} request on URL: {}", method, baseUrl + endpointUri);
    }

    private ApiClient setHttpVersion(HttpVersion httpVersion) {
        this.httpVersion = httpVersion;
        return this;
    }

    private ApiClient setConnectionTimeout(long connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
        return this;
    }

    private ApiClient setSocketTimeout(long socketTimeout) {
        this.socketTimeout = socketTimeout;
        return this;
    }

    private ApiClient setBaseUrl(URL baseUrl) {
        this.baseUrl = baseUrl;
        return this;
    }

    private ApiClient setFuse(Fuse fuse) {
        this.fuse = fuse;
        return this;
    }

    @Override
    public String toString() {
        return "ApiClient{" +
                "httpVersion=" + httpVersion +
                ", connectionTimeout=" + connectionTimeout +
                ", socketTimeout=" + socketTimeout +
                ", baseUrl=" + baseUrl +
                ", fuse=" + fuse +
                '}';
    }

    public static class Builder {

        private static final long DEFAULT_TIMEOUT_IN_SECONDS = 5L;

        private HttpVersion httpVersion = HttpVersion.V2;
        private long connectionTimeout = DEFAULT_TIMEOUT_IN_SECONDS;
        private long socketTimeout = DEFAULT_TIMEOUT_IN_SECONDS;
        private URL baseUrl;
        private Fuse fuse;

        public Builder(URL baseUrl) {
            this.baseUrl = baseUrl;
        }

        public Builder setHttpVersion(HttpVersion httpVersion) {
            this.httpVersion = httpVersion;
            return this;
        }

        public Builder setConnectionTimeout(long connectionTimeout) {
            this.connectionTimeout = connectionTimeout;
            return this;
        }

        public Builder setSocketTimeout(long socketTimeout) {
            this.socketTimeout = socketTimeout;
            return this;
        }

        public Builder setBaseUrl(URL baseUrl) {
            this.baseUrl = baseUrl;
            return this;
        }

        public Builder setFuse(Fuse fuse) {
            this.fuse = fuse;
            return this;
        }

        public ApiClient build() {
            return new ApiClient()
                    .setBaseUrl(this.baseUrl)
                    .setConnectionTimeout(this.connectionTimeout)
                    .setSocketTimeout(this.socketTimeout)
                    .setHttpVersion(this.httpVersion)
                    .setFuse(Optional.ofNullable(this.fuse).orElse(DefaultFuse.instance()));
        }

        public static Builder newBuilder(URL baseUrl) {
            return new Builder(baseUrl);
        }
    }
}
