package init5.productions.chainofresponsibility.handler;

import init5.productions.chainofresponsibility.message.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

import static init5.productions.chainofresponsibility.message.Message.Type.CRITICAL;

/**
 * @author Jakub Barski
 */
public class MailMessageHandler extends MessageHandlerAbstract {
    private static final Logger LOGGER = LogManager.getLogger(MailMessageHandler.class);
    private static final List<Message.Type> HANDLED_TYPES = List.of(CRITICAL);

    public MailMessageHandler() {
    }

    public MailMessageHandler(MessageHandler nextHandler) {
        super(nextHandler);
    }

    @Override
    protected void handleLogic(Message message) {
        LOGGER.info("Sending message of type [{}] as mail alert.", message.getType());
    }

    @Override
    protected boolean hasToHandle(Message message) {
        return HANDLED_TYPES.contains(message.getType());
    }
}
