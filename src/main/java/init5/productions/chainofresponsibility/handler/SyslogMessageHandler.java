package init5.productions.chainofresponsibility.handler;

import init5.productions.chainofresponsibility.message.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Jakub Barski
 */
public class SyslogMessageHandler extends MessageHandlerAbstract {
    private static final Logger LOGGER = LogManager.getLogger(SyslogMessageHandler.class);

    public SyslogMessageHandler() {
    }

    public SyslogMessageHandler(MessageHandler nextHandler) {
        super(nextHandler);
    }

    @Override
    protected void handleLogic(Message message) {
        LOGGER.info("Sending Message of type [{}] to syslog.", message.getType());
    }

    @Override
    protected boolean hasToHandle(Message message) {
        return true;
    }
}
