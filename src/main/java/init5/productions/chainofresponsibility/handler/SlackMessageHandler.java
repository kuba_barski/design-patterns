package init5.productions.chainofresponsibility.handler;

import init5.productions.chainofresponsibility.message.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

import static init5.productions.chainofresponsibility.message.Message.Type.CRITICAL;
import static init5.productions.chainofresponsibility.message.Message.Type.ERROR;

/**
 * @author Jakub Barski
 */
public class SlackMessageHandler extends MessageHandlerAbstract {
    private static final Logger LOGGER = LogManager.getLogger(SlackMessageHandler.class);
    private static final List<Message.Type> HANDLED_TYPES = List.of(ERROR, CRITICAL);

    public SlackMessageHandler() {
    }

    public SlackMessageHandler(MessageHandler nextHandler) {
        super(nextHandler);
    }

    @Override
    protected void handleLogic(Message message) {
        LOGGER.info("Sending Message of type [{}] to Slack channel.", message.getType());
    }

    @Override
    protected boolean hasToHandle(Message message) {
        return HANDLED_TYPES.contains(message.getType());
    }
}
