package init5.productions.chainofresponsibility.handler;

import init5.productions.chainofresponsibility.message.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Jakub Barski
 */
public class ElkMessageHandler extends MessageHandlerAbstract {
    private static final Logger LOGGER = LogManager.getLogger(ElkMessageHandler.class);

    public ElkMessageHandler() {
    }

    public ElkMessageHandler(MessageHandler nextHandler) {
        super(nextHandler);
    }

    @Override
    protected void handleLogic(Message message) {
        LOGGER.info("Pushing Message of type [{}] to ElasticSearch index.", message.getType());
    }

    @Override
    protected boolean hasToHandle(Message message) {
        return true;
    }
}
