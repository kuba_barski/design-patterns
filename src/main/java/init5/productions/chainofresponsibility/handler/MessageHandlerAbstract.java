package init5.productions.chainofresponsibility.handler;

import init5.productions.chainofresponsibility.message.Message;

import java.util.Optional;

/**
 * @author Jakub Barski
 */
public abstract class MessageHandlerAbstract implements MessageHandler {

    private MessageHandler nextHandler;

    public MessageHandlerAbstract() {
    }

    public MessageHandlerAbstract(MessageHandler nextHandler) {
        this.nextHandler = nextHandler;
    }

    @Override
    public void handle(Message message) {
        if (hasToHandle(message)) {
            handleLogic(message);
            message.incrementHandleCount();
        }
        Optional.ofNullable(nextHandler).ifPresent(handler -> handler.handle(message));
    }

    abstract protected void handleLogic(Message message);

    abstract protected boolean hasToHandle(Message message);
}
