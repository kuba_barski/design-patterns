package init5.productions.chainofresponsibility.handler;

import init5.productions.chainofresponsibility.message.Message;

/**
 * @author Jakub Barski
 */
public interface MessageHandler {

    void handle(Message message);
}
