package init5.productions.chainofresponsibility.message;

/**
 * @author Jakub Barski
 */
public interface Message {
    String getContent();

    Type getType();

    int getHandleCount();

    void incrementHandleCount();

    enum Type {
        INFO, WARNING, ERROR, CRITICAL
    }
}
