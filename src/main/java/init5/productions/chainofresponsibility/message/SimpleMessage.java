package init5.productions.chainofresponsibility.message;

/**
 * @author Jakub Barski
 */
public class SimpleMessage implements Message {

    private String content;
    private Type type;
    private int handleCount;

    private SimpleMessage(String content, Type type) {
        this.content = content;
        this.type = type;
        this.handleCount = 0;
    }

    public static SimpleMessage info(String content) {
        return new SimpleMessage(content, Type.INFO);
    }

    public static SimpleMessage warning(String content) {
        return new SimpleMessage(content, Type.WARNING);
    }

    public static SimpleMessage error(String content) {
        return new SimpleMessage(content, Type.ERROR);
    }

    public static SimpleMessage critical(String content) {
        return new SimpleMessage(content, Type.CRITICAL);
    }

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public Type getType() {
        return type;
    }

    @Override
    public int getHandleCount() {
        return handleCount;
    }

    @Override
    public void incrementHandleCount() {
        this.handleCount++;
    }
}
