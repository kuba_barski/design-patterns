package init5.productions.chainofresponsibility;

import init5.productions.chainofresponsibility.handler.*;
import init5.productions.chainofresponsibility.message.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Jakub Barski
 */
public class MessageSink {
    private static final Logger LOGGER = LogManager.getLogger(MessageSink.class);

    private MessageHandler chain;

    public MessageSink() {
        this.chain = new SyslogMessageHandler(new ElkMessageHandler(new SlackMessageHandler(new MailMessageHandler())));
    }

    public int process(Message message) {
        LOGGER.info("Processing message: {}.", message.getContent());
        chain.handle(message);
        LOGGER.info("Message was handled {} times.", message.getHandleCount());

        return message.getHandleCount();
    }
}
