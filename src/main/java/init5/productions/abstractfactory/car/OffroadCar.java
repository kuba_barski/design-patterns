package init5.productions.abstractfactory.car;

/**
 * @author Jakub Barski
 */
public class OffroadCar implements Car {

    @Override
    public String getModel() {
        return "Range Rover";
    }
}
