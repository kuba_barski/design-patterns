package init5.productions.abstractfactory.car;

/**
 * @author Jakub Barski
 */
public class CommonCar implements Car {

    public String getModel() {
        return "Skoda";
    }
}
