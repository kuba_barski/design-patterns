package init5.productions.abstractfactory.car;

/**
 * @author Jakub Barski
 */
public class RaceCar implements Car {

    public String getModel() {
        return "Ferrari";
    }
}
