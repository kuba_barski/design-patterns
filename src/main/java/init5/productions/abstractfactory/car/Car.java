package init5.productions.abstractfactory.car;

/**
 * @author Jakub Barski
 */
public interface Car {

    String getModel();
}
