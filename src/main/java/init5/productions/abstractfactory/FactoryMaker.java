package init5.productions.abstractfactory;

import init5.productions.abstractfactory.factory.CommonVehicleFactory;
import init5.productions.abstractfactory.factory.OffroadVehicleFactory;
import init5.productions.abstractfactory.factory.RaceVehicleFactory;
import init5.productions.abstractfactory.factory.VehicleFactory;

/**
 * @author Jakub Barski
 */
public class FactoryMaker {

    public enum VehicleType {
        RACE, COMMON, OFFROAD
    }

    public static VehicleFactory makeFactory(VehicleType type) {
        switch (type) {
            case RACE:
                return new RaceVehicleFactory();

            case COMMON:
                return new CommonVehicleFactory();

            case OFFROAD:
                return new OffroadVehicleFactory();

            default:
                throw new IllegalArgumentException("VehicleType [" + type + "] not supported!");
        }
    }
}
