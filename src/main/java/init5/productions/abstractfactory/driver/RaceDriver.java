package init5.productions.abstractfactory.driver;

/**
 * @author Jakub Barski
 */
public class RaceDriver implements Driver {

    public String getName() {
        return "Alonzo";
    }
}
