package init5.productions.abstractfactory.driver;

/**
 * @author Jakub Barski
 */
public class OffroadDriver implements Driver {

    @Override
    public String getName() {
        return "Hołowczyc";
    }
}
