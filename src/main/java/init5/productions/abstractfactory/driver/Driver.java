package init5.productions.abstractfactory.driver;

/**
 * @author Jakub Barski
 */
public interface Driver {

    String getName();
}
