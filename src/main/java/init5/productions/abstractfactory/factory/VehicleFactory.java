package init5.productions.abstractfactory.factory;

import init5.productions.abstractfactory.car.Car;
import init5.productions.abstractfactory.driver.Driver;

/**
 * @author Jakub Barski
 */
public interface VehicleFactory {

    Car produceCar();

    Driver produceDriver();
}
