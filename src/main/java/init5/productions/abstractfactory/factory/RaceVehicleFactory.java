package init5.productions.abstractfactory.factory;

import init5.productions.abstractfactory.car.Car;
import init5.productions.abstractfactory.car.RaceCar;
import init5.productions.abstractfactory.driver.Driver;
import init5.productions.abstractfactory.driver.RaceDriver;

/**
 * @author Jakub Barski
 */
public class RaceVehicleFactory implements VehicleFactory {

    @Override
    public Car produceCar() {
        return new RaceCar();
    }

    @Override
    public Driver produceDriver() {
        return new RaceDriver();
    }
}
