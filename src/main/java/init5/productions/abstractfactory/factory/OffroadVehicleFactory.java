package init5.productions.abstractfactory.factory;

import init5.productions.abstractfactory.car.Car;
import init5.productions.abstractfactory.car.OffroadCar;
import init5.productions.abstractfactory.driver.Driver;
import init5.productions.abstractfactory.driver.OffroadDriver;

public class OffroadVehicleFactory implements VehicleFactory {

    @Override
    public Car produceCar() {
        return new OffroadCar();
    }

    @Override
    public Driver produceDriver() {
        return new OffroadDriver();
    }
}
