package init5.productions.abstractfactory.factory;

import init5.productions.abstractfactory.car.Car;
import init5.productions.abstractfactory.car.CommonCar;
import init5.productions.abstractfactory.driver.CommonDriver;
import init5.productions.abstractfactory.driver.Driver;

/**
 * @author Jakub Barski
 */
public class CommonVehicleFactory implements VehicleFactory {

    @Override
    public Car produceCar() {
        return new CommonCar();
    }

    @Override
    public Driver produceDriver() {
        return new CommonDriver();
    }
}
