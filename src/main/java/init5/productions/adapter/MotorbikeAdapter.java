package init5.productions.adapter;

import init5.productions.adapter.car.Car;
import init5.productions.adapter.motorbike.Motorbike;

/**
 * @author Jakub Barski
 */
public class MotorbikeAdapter implements Car {

    private Motorbike motorbike;

    public MotorbikeAdapter(Motorbike motorbike) {
        this.motorbike = motorbike;
    }

    @Override
    public void openTrunk() {
        motorbike.openSaddlebag();
    }

    @Override
    public void putIntoTrunk(Object load) {
        motorbike.putIntoSaddlebag(load);
    }

    @Override
    public void closeTrunk() {
        motorbike.closeSaddlebag();
    }

    @Override
    public void drive() {
        motorbike.ride();
    }
}
