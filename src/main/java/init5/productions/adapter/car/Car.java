package init5.productions.adapter.car;

/**
 * @author Jakub Barski
 */
public interface Car {

    void openTrunk();

    void putIntoTrunk(Object load);

    void closeTrunk();

    void drive();
}
