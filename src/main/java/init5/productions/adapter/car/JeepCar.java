package init5.productions.adapter.car;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Jakub Barski
 */
public class JeepCar implements Car {

    private static final Logger LOGGER = LogManager.getLogger(JeepCar.class);

    @Override
    public void openTrunk() {
        LOGGER.info("Opening trunk");
    }

    @Override
    public void putIntoTrunk(Object load) {
        LOGGER.info("Putting {} into trunk", load.toString());
    }

    @Override
    public void closeTrunk() {
        LOGGER.info("Closing trunk");
    }

    @Override
    public void drive() {
        LOGGER.info("Driving");
    }
}
