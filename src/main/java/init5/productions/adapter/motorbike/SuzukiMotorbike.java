package init5.productions.adapter.motorbike;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Jakub Barski
 */
public class SuzukiMotorbike implements Motorbike {

    private static final Logger LOGGER = LogManager.getLogger(SuzukiMotorbike.class);

    @Override
    public void openSaddlebag() {
        LOGGER.info("Opening saddlebag");
    }

    @Override
    public void putIntoSaddlebag(Object load) {
        LOGGER.info("Putting {} into saddlebag", load.toString());
    }

    @Override
    public void closeSaddlebag() {
        LOGGER.info("Closing saddlebag");
    }

    @Override
    public void ride() {
        LOGGER.info("Riding");
    }
}
