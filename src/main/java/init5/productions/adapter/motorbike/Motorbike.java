package init5.productions.adapter.motorbike;

/**
 * @author Jakub Barski
 */
public interface Motorbike {

    void openSaddlebag();

    void putIntoSaddlebag(Object load);

    void closeSaddlebag();

    void ride();
}
