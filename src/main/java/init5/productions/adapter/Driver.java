package init5.productions.adapter;

import init5.productions.adapter.car.Car;

/**
 * @author Jakub Barski
 */
public class Driver {

    private Car car;

    public Driver(Car car) {
        this.car = car;
    }

    public void transportLoad(Object load) {
        car.openTrunk();
        car.putIntoTrunk(load);
        car.closeTrunk();
        car.drive();
    }
}
